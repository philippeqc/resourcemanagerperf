import plotly.graph_objects as go
version=["Original", ".NET Core 3.1", "TrimImage", "TrimImage 2"]
timeHdSec=[409.6793512, 308.0307778, 266.9060666,  210.56483]
timeHdMin=[6.82798918666667, 5.13326144666667, 4.44843444333333, 3.50941383333333]
timeSdSec=[24.7335687, 16.866288, 0, 0]
timeSdMin=[0.412226145, 0.2811048, 0, 0]
reference = timeHdSec[0]
ratioHd=[e/reference for e in timeHdSec]
reference = timeSdSec[0]
ratioSd=[e/reference for e in timeSdSec]

fig = go.Figure(data=[
    # go.Bar(x=version, y=timeHdSec, text=timeHdMin, textposition="auto"),
    # go.Bar(x=version, y=timeSdSec, text=timeSdMin, textposition="auto"),
    go.Bar(x=version, y=ratioHd, text=timeHdMin, textposition="auto"),
    go.Bar(x=version, y=ratioSd, text=timeSdMin, textposition="auto"),
])
fig.update_traces(texttemplate="%{text:.4s} mins", textposition='outside')
fig.update_layout(uniformtext_minsize=8, uniformtext_mode='hide')
fig.update_layout(
    xaxis = dict(title_text = "Version"),
    yaxis = dict(title_text = "Time (sec)"))
fig.layout.title.text = "Generating Buccaneer Bucks Ressources"
# fig.show()

import dash
import dash_core_components as dcc
import dash_html_components as html

app = dash.Dash()
app.layout = html.Div([
    dcc.Graph(figure=fig)
])

app.run_server(debug=False, use_reloader=False)  # Turn off reloader if inside Jupyter