import pandas as pd
import plotly.graph_objects as go

## sample df
df = pd.DataFrame({'Date': ['2010 - Q3','2010 - Q4','2011 - Q1','2011 - Q2','2011 - Q3','2011 - Q4'],
                   'Rate' : ['11.4','12.2','14.4','15.5','10.1','13.1'],
                   'Rate1': ['2.1','2.3','1.9','1.6','2.5','1.1']
                 })

## first and last bar are in lst
lst = ['2010 - Q3', '2011 - Q4']

## NOT NEEDED ##

## add a color column to the df, apply along row
## df['Color'] = df.apply(lambda x: 'rgb(222,0,0)' if x['Date'] in lst else 'rgb(0,222,0)', axis=1)
## clrs = 'rgb(222,0,0)'

fig = go.Figure(

            data=[

                  go.Bar(
                        x=df['Date'],
                        y=df['Rate'],
                        name='Natural Level'
                        ),

                  ],

            layout=go.Layout(

                title='Measuring excess demand and supply in the market.',

                xaxis=dict(
                    tickangle=90,
                    tickfont=dict(family='Rockwell', color='crimson', size=14)
                ),

                yaxis=dict(
                    title='Rate',
                    showticklabels=True
                ),

                barmode='stack',

            )
        ) 

## part of the dataframe in the lst
fig.add_trace(go.Bar(
                        x=df[df['Date'].isin(lst)]['Date'],
                        y=df[df['Date'].isin(lst)]['Rate1'],
                        name='Change1',
                        marker=dict(color='rgb(222,0,0)')
                        )

    )
fig.add_trace(go.Bar(
                        x=df[~df['Date'].isin(lst)]['Date'],
                        y=df[~df['Date'].isin(lst)]['Rate1'],
                        name='Change2',
                        marker=dict(color='rgb(0,222,0)')
                        )

    )

fig.show()