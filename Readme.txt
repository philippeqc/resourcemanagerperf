conda create --prefix ./envs dash pandas pandas-datareader ipython
conda activate ./envs


conda install -c conda-forge textblob
python -m textblob.download_corpora

# conda  install vaderSentiment
conda install nltk

puis
```
import nltk
nltk.download('vader_lexicon')
```

conda install -c conda-forge tweepy
conda install unidecode -y
