#
# To point to the proper version of ResourceManagerConsole:
# $Env:path = "D:\code\Tools.v2\ResourceManager\ResourceManagerConsole\bin\Debug\netcoreapp3.1\;" + $Env:path
#



$fizzBuzz2 = @{
    Themes = @(
        @{
            GameTheme = "Buccaneer Bucks";
            Format    = "Portrait";
            AssetPath = "/GameThemes/BuccaneerBucks/Assets/Portrait";
        },
        @{
            GameTheme = "Moonlight Dragon";
            Format    = "Portrait"
            AssetPath = "/GameThemes/MoonlightDragon/Assets/Portrait"
            # Assets    = @(
            #     "/GameThemes/MoonlightDragon/Assets/Portrait/MDFireworkBonusHDP.lres",
            #     "/GameThemes/MoonlightDragon/Assets/Portrait/MDFireworkBonusStaticHDP.lres"
            # #     "/GameThemes/MoonlightDragon/Assets/Portrait/MDMainBannerHDP.lres",
            # #     "/GameThemes/MoonlightDragon/Assets/Portrait/MDMainFSHDP.lres",
            #     # "/GameThemes/MoonlightDragon/Assets/Portrait/MDMainExtraScreenCommonHDP.lres"
            #         )
                     },
        @{
            GameTheme = "Pick Axe Pete";
            Format    = "Portrait"
            AssetPath = "/GameThemes/PickAxePete/Assets/Portrait"
        # Assets    = @(
        #     "/GameThemes/PickAxePete/Assets/Portrait/PAPMainStaticHDP.lres",
        #     "/GameThemes/PickAxePete/Assets/Portrait/PAPMatch3BonusBannersHDP.lres",
        #     "/GameThemes/PickAxePete/Assets/Portrait/PAPMatch3BonusSecondaryScreenHDP.lres"
        # )
        },
        @{
            GameTheme = "Space Force";
            Format    = "Portrait"
            AssetPath = "/GameThemes/SpaceForce/Assets/Portrait"
        # Assets    = @(
        #     "/GameThemes/SpaceForce/Assets/Portrait/SFMainBannerHDP.lres",
        #     "/GameThemes/SpaceForce/Assets/Portrait/SFMainFSHDP.lres",
        #     "/GameThemes/SpaceForce/Assets/Portrait/SFRocketBonusBannersHDP.lres"
        # )
    },
        @{
            GameTheme = "Wolves Gone Wild";
            Format    = "Portrait";
            AssetPath = "/GameThemes/WolvesGoneWild/Assets/Portrait"
            # Assets    = @(
            #     "/GameThemes/WolvesGoneWild/Assets/Portrait/WGWMainBannerHDP.lres",
            #     "/GameThemes/WolvesGoneWild/Assets/Portrait/WGWMainFSHDP.lres",
            #     "/GameThemes/WolvesGoneWild/Assets/Portrait/WGWMainStaticHDP.lres"
            # )
    }
    )
}

Function Foo
{
    Param (
        [switch]$Verbose
    )
    
    $file = Join-Path $PSScriptRoot "ResourceManager\out.csv"

    if (-Not (Test-Path $file)) 
    {
        # Create the file with header
        "lib version,improvement,Computer,Date,Game Theme,Format,Asset Name,duration" | Out-File -FilePath $file
    }

    $root = "D:\\Reference_ResourceManager\\Source"

    $version = (-split (& $app --version | Select-String -Pattern "ResourceManagerLib"))[1]
    $outPath = Join-Path "D:\Code\out\fizz\" $version
    # if (Test-Path $outPath) {
    #     Remove-Item -Force -Recurse $outPath
    # }
    # mkdir -Force $outPath |out-null

    if($Verbose -eq $true) {
        Write-Host $(Get-Date).DateTime
    }

    foreach ($theme in $fizzBuzz2.Themes) {
        if($theme.Assets.Count -gt 0) {
            # Get lres from the passed list.
            $assets = $(foreach($asset in $theme.Assets) {Get-ChildItem $(Join-Path $root $asset) -Filter *.lres}).FullName
            if($Verbose -eq $true) {
                Write-Host $($theme.GameTheme) "using assets:"
                foreach ($asset in $assets) {
                    Write-Host $asset
                }
            }
        } else {
            # Get all lres in the directory.
            $assets = $(Get-ChildItem $(Join-Path $root $theme.AssetPath) -Filter *.lres).FullName
            if($Verbose -eq $true) {
                Write-Host ("Scanning folder $($theme.AssetPath)")
            }
        }
        $themeOutPath = Join-Path $outPath $($theme.GameTheme)
        mkdir -Force $themeOutPath |out-null
    
        foreach ($asset in $assets) { 
            $src = $(Get-ChildItem $asset)
            $dst = (Join-Path $themeOutPath $src.BaseName) + ".rez"
            Write-Host "Working on" $dst "from" $src
            if($Verbose -eq $true) {
                Write-Host $app -S -K -L -o $dst $src
            }
            # & $app -S -K -L -o $dst $src ; 
            $duration = (Measure-Command { & $app -S -K -L -o $dst $src }).TotalSeconds; 
            "$version,$improvement,$($Env:COMPUTERNAME),$(Get-Date).ToString("yyyy-MM-dd HH:mm"),$($theme.GameTheme),$($theme.Format),$($src.BaseName),$duration" | Out-File -FilePath $file -Append
        }
    }
}

# # $app = "c:\\utils\\ResourceManager\\ResourceManagerConsole.exe"
# # $improvement="Original"
# # Foo

# $app = "D:\\Code\\ResourceManagerConsole\\1.15.4.0\\ResourceManagerConsole.exe"
# $improvement="Reference"
# Foo

# $app = "D:\\Code\\ResourceManagerConsole\\1.15.4.1\\ResourceManagerConsole.exe"
# $improvement=".Net Core 3.1"
# Foo

# $app = "D:\\Code\\ResourceManagerConsole\\1.15.4.2\\ResourceManagerConsole.exe"
# $improvement="Refactoring"
# Foo

# $app = "D:\\Code\\ResourceManagerConsole\\1.15.4.3\\ResourceManagerConsole.exe"
$app = "D:\\Code\\Toolsb\\ResourceManager\\ResourceManagerConsole\\bin\\Release\\netcoreapp3.1\\ResourceManagerConsole.exe"
$improvement=".rgbReserved"
Foo

# $app = "D:\\Code\\Tools\\ResourceManager\\ResourceManagerConsole\\bin\\Debug\\netcoreapp3.1\\ResourceManagerConsole.exe"
$app = "D:\\Code\\Tools\\ResourceManager\\ResourceManagerConsole\\bin\\Release\\netcoreapp3.1\\ResourceManagerConsole.exe"
$improvement="TrimImageRect"
Foo

