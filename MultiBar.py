# conda create --prefix ./envs dash pandas pandas-datareader ipython pylint

import glob
import os
import plotly.express as px
import pandas as pd

all_files = glob.glob(os.path.join("./ResourceManager", "*.csv"))     # advisable to use os.path.join as this makes concatenation OS independent

df_from_each_file = (pd.read_csv(f) for f in all_files)
dfraw = pd.concat(df_from_each_file, ignore_index=True)
df = dfraw.reset_index().sort_values(['lib version'])

# Compute duration ration vs first version 
df = (
    df
    .assign(Ratio=df.groupby(['Asset Name'])['duration'].transform(lambda group: group/group.iloc[0]))
    .sort_index()
)

# df = px.data.tips()
# fig = px.bar(df, x="sex", y="total_bill", color="smoker", barmode="group")
# fig1 = px.bar(df, x="lib_version", y="duration", color="GameTheme")
# fig1.show()
# fig2 = px.bar(df, x="lib version", y="duration", color="GameTheme", barmode="group")

# # header
# # lib version,improvement,GameTheme,type,duration
# fig = px.bar(df, x="improvement", y="duration", color="GameTheme", barmode="group")
# fig.show()


df.sort_values(by=['lib version', 'Game Theme', 'Asset Name'], inplace=True)

# header
# lib version,improvement,Game Theme,Format,Asset Name,duration
fig = px.bar(df, x="improvement", y="duration", color="Game Theme", barmode="group", 
    hover_name="Asset Name", hover_data=["Format", "Ratio"])

fig.update_layout(
    yaxis=dict(
        title='Duration (seconds)',
        titlefont_size=16,
    ),
    bargap=0.15, # gap between bars of adjacent location coordinates.
    bargroupgap=0.1 # gap between bars of the same location coordinate.
)


# marker_line_color='rgb(8,48,107)',
#                   marker_line_width=1.5, opacity=0.6
fig.show()
